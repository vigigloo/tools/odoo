variable "backup_schedule" {
  type    = string
  default = "25 3 * * *"
}

variable "backup" {
  type    = bool
  default = false
}

variable "backup_subfolder" {
  type    = string
  default = "odoo-postgresql"
}

variable "backup_endpoint" {
  type    = string
  default = null
}
variable "backup_bucket" {
  type    = string
  default = null
}
variable "backup_region" {
  type    = string
  default = null
}
variable "backup_secret_key" {
  type    = string
  default = null
}
variable "backup_access_key" {
  type    = string
  default = null
}
variable "backup_user" {
  type    = string
  default = null
}
variable "backup_password" {
  type    = string
  default = null
}

locals {
  endpoint = var.backup_endpoint != null ? var.backup_endpoint : "https://${var.backup_bucket}.s3.${var.backup_region}.scw.cloud"
}

resource "kubernetes_cron_job" "odoo_backup_postgresql" {
  count = var.backup ? 1 : 0
  metadata {
    name      = "odoo-backup"
    namespace = var.namespace
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 10
    schedule                      = var.backup_schedule
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10

    job_template {
      metadata {
        annotations = {}
        labels      = {}
      }

      spec {
        backoff_limit = 3

        template {
          metadata {
            annotations = {}
            labels      = {}
          }
          spec {
            container {
              name  = "odoo-backup"
              image = "registry.gitlab.com/vigigloo/tools/odoo/backup:latest"
              resources {
                limits = {
                  memory = "512Mi"
                }
                requests = {
                  cpu = "150m"
                }
              }

              env {
                name  = "NAME"
                value = var.backup_subfolder
              }

              env {
                name  = "S3_REGION"
                value = var.backup_region
              }

              env {
                name  = "S3_ENDPOINT_URL"
                value = local.endpoint
              }

              env {
                name  = "S3_ACCESS_KEY"
                value = var.backup_access_key
              }

              env {
                name  = "S3_SECRET_KEY"
                value = var.backup_secret_key
              }

              env {
                name  = "PGSQL_HOST"
                value = "${var.chart_name}-postgresql-hl.${var.namespace}.svc.cluster.local"
              }

              env {
                name  = "PGSQL_USER"
                value = var.backup_user
              }

              env {
                name  = "PGSQL_PASSWORD"
                value = var.backup_password
              }

              volume_mount {
                name       = "data"
                mount_path = "/data"
                read_only  = true
              }
            }

            affinity {
              pod_affinity {
                required_during_scheduling_ignored_during_execution {
                  label_selector {
                    match_expressions {
                      key      = "app.kubernetes.io/name"
                      operator = "In"
                      values   = [var.chart_name]
                    }
                  }
                  topology_key = "kubernetes.io/hostname"
                }
              }
            }

            volume {
              name = "data"
              persistent_volume_claim {
                claim_name = length(regexall("odoo", var.chart_name)) > 0 ? trimsuffix(substr(var.chart_name, 0, 63), "-") : trimsuffix(substr("${var.chart_name}-odoo", 0, 63), "-")
              }
            }
          }
        }
      }
    }
  }
}

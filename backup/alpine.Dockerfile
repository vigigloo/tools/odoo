FROM alpine:3.19

ENV NAME=odoo
ENV PGSQL_PORT=5432

RUN apk --no-cache add bash postgresql16-client py3-pip rsync tar && pip3 install --break-system-packages --no-cache-dir awscli awscli-plugin-endpoint

COPY ./backup.sh /usr/local/bin/backup.sh
RUN chmod +x /usr/local/bin/backup.sh

CMD ["./usr/local/bin/backup.sh"]

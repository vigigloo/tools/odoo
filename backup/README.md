# Odoo backup

Copy data from volume */data*, dump all databases and send archive to S3 object storage (s3://backups).

## Environment

| Name              | Description                                       | Default    |
 |-------------------|---------------------------------------------------|------------|
| NAME              | Defines archive name                              | odoo       |
| S3_REGION         | Specifies the S3 Region to send the request to    |            |
| S3_ACCESS_KEY     | The S3 access key to use for connection           |            |
| S3_SECRET_KEY     | The S3 secret key to use for connection           |            |
| S3_ENDPOINT_URL   | The S3 endpoint url to use for connection         |            |
| PGSQL_PORT        | PostgreSQL port number to use                     | 5432       |
| PGSQL_HOST        | PostgreSQL host to use                            |            |
| PGSQL_USER        | PostgreSQL Credentials username                   |            |
| PGSQL_PASSWORD    | PostgreSQL Credentials password                   |            |
